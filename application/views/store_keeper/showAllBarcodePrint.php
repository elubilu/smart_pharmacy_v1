<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Excellent Shoe</title>

    <!-- Custom Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_table.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>" />

     <!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/table.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-barcode.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
	
	<script type="text/javascript">
		$('.forselect2').select2();
	</script>
 	<style type="text/css" media="all">
	    
		#page-wrapper {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px;
		}
	 	@media print{
			.with_print{
	            display: none;
	        }
	        .page-break	{
				display: block; 
				page-break-before: auto;		
			}
			.container{
				width: 100%;
				padding-right: 0px;
    			padding-left: 0px;
			}
	    }
    </style>
</head>

<body>
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container">
				<div class="row with_print">
					<div class="col-md-6 m-bottom-25">
						<h3 class="page-header">Print Barcode</h3>
					</div>
					<div class="col-md-6">
						<div class="pull-right m-top-25">

							<button class="btn btn-success with_print" name="print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>

							<a class="btn btn-danger" href="<?php echo base_url('storeKeeper/printBarcode'); ?>"></i> Back</a>

						</div>
					</div>
				</div>	
				<div class="row" id="">
					<div class="col-md-12 col-xs-12">
						<div class="page-break barcodeDiv"></div>	
					</div>
				</div>
				
				<?php 
					$cart=$this->cart->contents();
					foreach ($cart as $item) { 
						for($i=0; $i< $item['qty']; $i++){ 
				?>
				<div class="col-md-2 col-xs-2 col-sm-2 countRow" style="font-size:12px;padding:2px;margin-bottom:0.5px;font-weight:700;">
					<div class="thumbnail" style="margin-bottom:0px;border:1px dotted;border-radius:0px;">
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Excellent</p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;"><?php echo $item['name'] ?></p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Price:<?php echo  $item['price'] ?></p>
						<p style="text-align:center;margin:0px auto;" class="printFont barcodeTarget<?php echo $item['productBarcode'] ?>"></p>
						
					</div>
				</div>
				<div class="col-md-2 col-xs-2 col-sm-2 countRow" style="font-size:12px;padding:2px;margin-bottom:0.5px;font-weight:700;">
					<div class="thumbnail" style="margin-bottom:0px;border:1px dotted;border-radius:0px;">
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Excellent</p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;"><?php echo $item['name'] ?></p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Price:<?php echo  $item['price'] ?></p>
						<p style="text-align:center;margin:0px auto;" class="printFont barcodeTarget<?php echo $item['productBarcode'] ?>"></p>
						
					</div>
				</div>
				<div class="col-md-2 col-xs-2 col-sm-2 countRow" style="font-size:12px;padding:2px;margin-bottom:0.5px;font-weight:700;">
					<div class="thumbnail" style="margin-bottom:0px;border:1px dotted;border-radius:0px;">
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Excellent</p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;"><?php echo $item['name'] ?></p>
						<p style="text-align:center;margin:0px;font-size:12px;font-weight:700;">Price:<?php echo  $item['price'] ?></p>
						<p style="text-align:center;margin:0px auto;" class="printFont barcodeTarget<?php echo $item['productBarcode'] ?>"></p>
						
					</div>
				</div>
					
				<?php } ?>
				<script>
					Bcart=<?php echo json_encode($item) ; ?>;
					generateBarcode(Bcart['productBarcode']);
					function generateBarcode(value){
						var value = value;
						var btype = "code39";
						//var renderer = "css";			
						var settings = {
							output: "css",
							bgColor: "#FFF",
							color: "#000",
							barWidth: 1,
							barHeight: 23.5
						};
						$(".barcodeTarget"+value).html("").show().barcode(value, btype, settings);
					}
				</script>

				<?php } ?>

			</div>
		</div>
    </div>
    <script type="text/javascript">
       
		$(window).on("load", function(){
		  window.print();
		});
    </script>
	
</body>

</html>

