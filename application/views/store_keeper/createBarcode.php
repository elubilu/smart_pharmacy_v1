<?php include('header.php');?>
				
			<div class="row with_print">
				<div class="col-md-12">
					<h3 class="page-header">Print Barcode</h3>
				</div>
			</div>
<div class="row">
  <div class="col-md-12">
	 <ol class="breadcrumb">
		<li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
		<li class="active">Print Barcode</li>
	 </ol>
  </div>
</div>
			<div class="row with_print">
				<div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-body">
							<?php $attributes = array('id' => 'barcodeForm');
								echo form_open('',$attributes); ?>
								<div class="row">
									<div class="col-md-6">
										<label>Product</label>
										<input type="text" class="form-control barcodeProductID" name="productBarcode" id="productBarcode" required="required">
										<div style="" id="barcodeError" class="warningSize red-text"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Product Quantity</label>
											<input type="text" class="form-control barcodeProductQuantity">
										</div>
									</div>
								</div>
								<!--<div class="row pull-right">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary barcodeButton" id="addBarcodePrintbutton"  >Add</button>
										<button class="btn btn-warning with_print" name="print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
									</div>
								</div>-->
								<div class="row">
									<div class="col-md-12">
										<!--<button type="submit" class="btn btn-primary barcodeButton" id="addBarcodePrintbutton"  >Add</button>-->
										<button type="button" class="btn btn-primary barcodeButton" id="addBarcodeToCart"  >Add</button>
									</div>
								</div>							
								<div class="row m-top-25">
									<div class="col-md-12">
										<table class="table table-striped" >
											<thead>
												<tr>
													<th>Product Name</th>
													<th>Quantity</th>
													<th>Remove</th>
												</tr>
											</thead>
											<tbody id="productBarcodeTable">
												
											</tbody>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<a class="btn btn-warning with_print" href="<?php echo base_url('storeKeeper/showAllBarcodePrint') ?>" name="print" ><i class="fa fa-print"></i> Print</a>
									</div>
								</div>	
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="">
				<div class="col-md-12 col-xs-12">
					<div class="page-break barcodeDiv"></div>	
				</div>
			</div>
		<?php include('footer.php');?>