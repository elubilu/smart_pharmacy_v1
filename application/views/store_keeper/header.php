<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Excellent Shoe</title>

    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jQuery-UI-v1.11.0.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_table.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
	
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('StoreKeeper/index'); ?>">Excellent Shoe</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="<?php  echo base_url('StoreKeeper/myAccount'); ?>"><i class="fa fa-user fa-fw"></i> My Account</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php  echo base_url('login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="text-center user-img hidden-xs hidden-sm">
                            <img src="http://localhost/mama_project/assets/images/forhad mama1.jpg" alt="" class="img-responsive img-circle"> <br> User: Riaz Ahmed
                        </li>-->
                        <li>
                            <a href="<?php echo base_url('StoreKeeper/index'); ?>"><i class="fa fa-bar-chart fa-fw"></i> Dashboard</a>
                        </li>
                       
                        <li>
                            <a href="#"><i class="fa fa-product-hunt fa-fw"></i> Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('StoreKeeper/addProductToStock'); ?>"> Add Product to Stock</a>
                                    <!--<a href="<?php // echo base_url('super_admin/addProductToStock'); ?>"> Add Product to Stock</a>-->
                                </li>
                                <li>
                                    <a href="<?php echo base_url('StoreKeeper/printBarcode'); ?>">Print Barcode</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('StoreKeeper/viewProduct'); ?>">View Product</a>
                                </li>
                            </ul>
                        </li>
						
						<li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('StoreKeeper/showInventory'); ?>"> Show Inventory</a>
                                    <a href="<?php echo base_url('StoreKeeper/productSaleReport'); ?>"> Product Sale Report</a>
                                    <a href="<?php echo base_url('StoreKeeper/returnReport'); ?>"> Return Report</a>
                                    <a href="<?php echo base_url('StoreKeeper/productUpdateStokeReport'); ?>"> Product Update Stock Report</a>
                                </li>
                            </ul>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-snowflake-o fa-fw"></i> Other<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('StoreKeeper/addNewProduct'); ?>"> Add New Product</a>
                                    <a href="<?php echo base_url('StoreKeeper/allSupplier'); ?>"> All Supplier</a>
                                    <a href="<?php echo base_url('StoreKeeper/allProductGroup'); ?>"> All Product Group</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">