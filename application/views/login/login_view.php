﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Excellent Shoe</title>

    <!-- Custom Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div>
        <?php echo $this->session->flashdata('login'); ?>
    </div>


    <?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
    <div class="alert alert-success">
        <strong>Success!</strong>
        <?php echo $this->session->flashdata('feedback_successfull'); ?>
    </div>
    <?php } ?>


    <?php echo form_open(); 
		
			/*echo form_input(['name'=>'user_id','placeholder'=>'User ID','value'=>set_value('user_id')]); 
			echo form_error('user_id');
			echo form_password(['name'=>'pass','placeholder'=>'Your Password']); 
			echo form_error('pass');
			echo form_submit(['name'=>'login', 'value'=>'Login']) ;
			echo form_reset(['name'=>'reset', 'value'=>'Reset']) ;
			
			echo form_close();*/
		?>
		
	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Log In</h3>
                    </div>
					<div id="result" style="display: none" class="text-xs-center text-sm-center text-md-center text-lg-center text-xl-center red-text">
						<center><p class="label_output" id="value"></p></center>
					</div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
									<?php echo form_input(['name'=>'email', 'id' => 'name', 'placeholder' => 'E-mail', 'class'=>'form-control', 'value'=>set_value('email')]); 
									echo form_error('email'); ?>
									<div style="" id="email" class="warningSize red-text"></div>
                                </div>
                                <div class="form-group">
									<?php echo form_password(['name'=>'password', 'id' => 'pwd', 'placeholder' => 'Password', 'class'=>'form-control']); 
									echo form_error('password'); ?>
									<div style="" id="password" class="warningSize red-text"></div>
                                </div>
                                <button type="submit" class="btn btn-default" value="Login" name="login" id="submit">Login</button>
                                <button type="submit" class="btn btn-danger" value="Reset" name="login">Reset</button>
								<?php echo form_close();?>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	

    <!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
	<script type="text/javascript">
        // Ajax post
        $(document).ready(function() {
            $("#submit").click(function(event) {
                event.preventDefault();
                var email = $("input#name").val();
                var password = $("input#pwd").val();
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + "login/user_login",
                    dataType: 'json',
                    data: {
                        email: email,
                        password: password
                    },
                    success: function(res) {
                        if (res) {
                            // Show Entered Value
                            if (res.status === true)
                                document.location.href = res.redirect;
                            else {
                                jQuery("div#result").show();
                                jQuery("#value").html(res.errors);
                                jQuery("#email").html(res.email);
                                jQuery("#password").html(res.password);
                                //jQuery("div#value_pwd").html(res.pwd);
                            }
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>