<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Return Percentage Rate</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Return Percentage Rate</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Current Return Value Deduction Rate</label>
                        <input class="form-control removeDisabled" name="currentReturnValueDeductionRate" id="" value="" disabled>
                     </div>
                  </div>
               </div>           
               <div class="row">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-primary" id="removeDisabledButton"><i class="fa fa-pencil"></i> Edit</button>
                     <button type="submit" class="btn btn-success addDisabled hidden"><i class="fa fa-thumbs-up"></i> Save</button>
                     <button type="button" class="btn btn-warning addDisabled hidden" id="addDisabledButton"><i class="fa fa-times"></i> Cancel</button>
                  </div>
               </div>       
            </div>
         </div>
      </div>
   </div>
<?php echo form_close(); ?> 
<?php include('footer.php') ?>