<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Products</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Products</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>

   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-md-12">
                     <div class="pull-right">
                        <a href="<?= base_url('manager/addNewProduct')?>" class="btn btn-warning"> Add Product</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
               </div>  
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <option value="1">1</option>             
                           <option value="2">2</option>             
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                   <div class="col-md-12">
                      <button type="submit" class="btn btn-primary">Search</button>
                   </div>
                </div>	 
            </div>
         </div>
      </div>      
   </div>

   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info filterable">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-md-6">
                     <h4>Searched Product</h4>
                  </div>
                  <div class="col-md-6">
                     <div class="pull-right p-top-20">
                        <button id="filter_button" class="btn btn-warning btn-filter with_print" ><i class="fa fa-filter"></i> Filter
                        </button>
                     </div>                     
                  </div>
               </div>
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <table class="table table-striped">
                        <thead>
                           <tr class="active filters">
                              <th>
                                 <input type="text" class="form-control" placeholder="Barcode" disabled data-toggle="true" id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Brand /  Product Name" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Group" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Dosage Form" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Manufacturer" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Purchase Price" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="MRP" disabled id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Stock Quantity" disabled id="">
                              </th>
                              <th>
                                 <span >View</span>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($data as  $value) {
                              # code...
                           ?>
                           <tr>
                             <td><?php echo $value->productBarcode?></td>
                             <td><?php echo $value->productName?></td>
                             <td><?php echo $value->group_info_productGroupId?></td>
                             <td><?php echo $value->dosage_info_productDosageFormId?></td>
                             <td><?php echo $value->manufacturer_info_productManufacturerId?></td>
                             <td><?php echo $value->productPurchasePrice?></td>
                             <td><?php echo $value->productMRP?></td>
                             <td><?php //echo $value->productId?></td>
                              <td><a href="<?php echo base_url("manager/viewProduct/{$value->productId}")?>" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></a></td>
                           </tr> 
                          <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>                  
            </div>   
         </div>   
      </div>
   </div> 
<?php include('footer.php') ?>