<?php include('header.php') ?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Add New Medicine Group</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add New Medicine Group</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
   <?php echo form_open('manager/storeMedicineGroup'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Group Name</label>
                        <input type="text" class="form-control" name="medicineGroupName" value="">
                        <div class="errorClass"><?php echo form_error('medicineGroupName'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Contains</label>
                        <input type="text" class="form-control" name="medicineGroupContains" value="">
                        <div class="errorClass"><?php echo form_error('medicineGroupContains'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Note</label>
                        <textarea name="medicineGroupNote" class="form-control"></textarea>
                        <div class="errorClass"><?php echo form_error('medicineGroupNote'); ?></div>
                     </div>
                  </div>                 
               </div>  

               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Add</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div> 
   <?php echo form_close(); ?>
<?php include('footer.php') ?>