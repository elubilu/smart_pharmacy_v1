		
		
		
		</div>
    </div>

    <!-- jQuery -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-timepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/table.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/mousetrap.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>	
	<script type="text/javascript">
		$('.forselect2').select2();
	</script>
	
	<script type="text/javascript">
		// Ajax post
		$(document).ready(function() {
			$(".submit").click(function(event) {
				event.preventDefault();
				var productBarcode = $("input#productBarcode").val();
				var quantity = $("input#quantity").val();
				if(quantity<1) quantity=1;
				//alert(productBarcode);
				jQuery("#quantity_errors").hide();
				jQuery("#quantity_errors").html("");
				if(productBarcode){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/ajax_add_cart",
						dataType: 'json',
						data: {
							productBarcode: productBarcode,
							quantity: quantity
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									//alert("yes");
									$("#tableHeaderID").show();
									document.getElementById("myForm").reset();
									if(res.past_buy === true){
										var product_id = res.product_id;
										var quantity = res.quantity;
										var price = res.price;
										var subtotal = price*quantity;
										var qid="qty"+product_id;
										var subid="subtotal_row"+product_id;
										var old_qty=$("#"+qid).val();
										var old_subtotal=old_qty*price;
										var old_totalQty=$("#totalQuantityFieldID").val();
										old_totalQty=(+old_totalQty)+(+quantity);
										old_totalQty=(+old_totalQty)-(+old_qty);
										
										//* for total quantity *//
										$("#totalQuantityFieldID").val(old_totalQty);
										$("#"+qid).val(quantity);
										$("#"+subid).html(subtotal);
										
										//* for total price *//
										var old_totalPrice=$("#totalPriceFieldID").val();
										old_totalPrice=(+old_totalPrice)-(+old_subtotal);
										old_totalPrice=(+old_totalPrice)+(+subtotal);
										$("#totalPriceFieldID").val(old_totalPrice);
										$("#netPayable").val(old_totalPrice);
										
									}
									else{
										
										var product_id = res.product_id;
										var quantity = res.quantity;
										var total_quantity = res.total_quantity;
										var price = res.price;
										var name = res.name;
										var barcode = res.barcode;
										var rowid = res.rowid;
										var subtotal = price*quantity;
										//alert(barcode);
										var table = document.getElementById("productTable");
										var table_len = (table.rows.length);
										var row = table.insertRow(table_len).outerHTML = "<tr id='row" + product_id + "' class='active'><td id='barcode_row" + table_len + "'>" + barcode + "</td><td id='name_row" + table_len + "'>" + name + "</td><td style='width:20%' id='quantity_row" + table_len + "'><div class='quantity clearfix'> <button type='button' class='minus btn btn-sm btn-primary' onclick='minusQuantity(\""+ rowid + "\","+product_id+","+price+")'><i class='fa fa-minus'></i></button> <input type='number' style='width:50px' name='quantity' value='"+quantity+"' class='quantity' id='qty" + product_id + "' readonly='' /> <button type='button' class='plus btn btn-sm btn-primary' onclick='plusQuantity(\""+ rowid + "\","+total_quantity+","+product_id+","+price+")'><i class='fa fa-plus'></i></button></div></td><td id='price_row" + product_id + "'>" + price + "</td><td id='subtotal_row" + product_id + "'>" + subtotal + "</td><td id='remove_row" + table_len + "' style='width:5%;text-align:center;' ><a id='my_cart_table_close' onclick='delete_row(\""+ rowid + "\","+product_id+","+price+")' ><i class='fa fa-remove'></i></a></td></tr>";
										//* for total quantity *//
										var old_totalQty=$("#totalQuantityFieldID").val();
										old_totalQty=(+old_totalQty)+(+quantity);
										$("#totalQuantityFieldID").val(old_totalQty);
										//* for total price *//
										var old_totalPrice=$("#totalPriceFieldID").val();
										old_totalPrice=(+old_totalPrice)+(+subtotal);
										$("#totalPriceFieldID").val(old_totalPrice);
										$("#netPayable").val(old_totalPrice);
	
									}
									
								}
								else {
									jQuery("#quantity_errors").show();
									jQuery("#quantity_errors").html(res.errors);
									
								}
							}
						}
					});
				}
			});
		});
	</script>
	<script type="text/javascript">// Plus Quantity
			function plusQuantity(row_id,total_qty,len,price){
				//alert(total_qty);
				
				var qid="qty"+len;
				var subid="subtotal_row"+len;
				var input_val = $("#"+qid).val();
				input_val=++input_val;
				var sub_total=input_val*price;
				  //alert(sub_total);
				 // alert(input_val);
				if(total_qty>=input_val){
					//event.preventDefault();
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/plus_product",
						dataType: 'json',
						data: {
							row_id: row_id,
							qty: input_val,
						},
						success: function(res) {
							if (res) {
								// Show Entered Value
								if (res.status === true) {
									//alert(sub_total);
									$("#"+qid).val(input_val);
									$("#"+subid).html(sub_total);
									var old_totalQty=$("#totalQuantityFieldID").val();
									old_totalQty=++old_totalQty;
									$("#totalQuantityFieldID").val(old_totalQty);
									//* for total price *//
									var old_totalPrice=$("#totalPriceFieldID").val();
									//alert(price);
									old_totalPrice=(+old_totalPrice)+(+price);
									$("#totalPriceFieldID").val(old_totalPrice);
									$("#netPayable").val(old_totalPrice);
									
									
								} else {
									//jQuery("#quantity_errors").show();
									//jQuery("#quantity_errors").html(res.errors);
								}
							}
						}
					});
					
				}
			}
	</script>
	<script type="text/javascript">// Plus Quantity
			function minusQuantity(row_id,len,price){
				//event.preventDefault();
				var qid="qty"+len;
				var subid="subtotal_row"+len;
				var input_val = $("#"+qid).val();
				input_val=--input_val;
				var sub_total=input_val*price;
				if(input_val>0){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/minus_product",
						dataType: 'json',
						data: {
							row_id: row_id,
							qty: input_val,
						},
						success: function(res) {
							if (res) {
								// Show Entered Value
								if (res.status === true) {
									$("#"+qid).val(input_val);
									$("#"+subid).html(sub_total);
									var old_totalQty=$("#totalQuantityFieldID").val();
									old_totalQty=--old_totalQty;
									$("#totalQuantityFieldID").val(old_totalQty);
									//* for total price *//
									var old_totalPrice=$("#totalPriceFieldID").val();
									old_totalPrice=(+old_totalPrice)-(+price);
									$("#totalPriceFieldID").val(old_totalPrice);
									$("#netPayable").val(old_totalPrice);
									
								} else {
									//jQuery("#quantity_errors").show();
									//jQuery("#quantity_errors").html(res.errors);
								}
							}
						}
					});
					
				}
			}
	</script>
	<script type="text/javascript">
		function delete_row(row_id,no,price) {
			//event.preventDefault();
			//alert(discount);
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "autocomplete/remove",
				dataType: 'json',
				data: {
					row_id: row_id,
					//quantity: quantity,
				},
				success: function(res) {
					if (res) {
						// Show Entered Value
						if (res.status === true) {
							
							var qid="qty"+no;
							var input_qty = $("#"+qid).val();
							//alert(input_qty);
							var sub_total=(+input_qty)*price;
							//var totalPrice=$("#totalInputVal").val();
							//totalPrice=(+totalPrice)-(+sub_total);
							//$("#totalInputVal").val(totalPrice);
							//$(".totalAmountShow").html("Total Amount: "+totalPrice+" ৳");
							var old_totalQty=$("#totalQuantityFieldID").val();
							old_totalQty=(+old_totalQty)-(+input_qty);
							$("#totalQuantityFieldID").val(old_totalQty);
							//* for total price *//
							var old_totalPrice=$("#totalPriceFieldID").val();
							old_totalPrice=(+old_totalPrice)-(+sub_total);
							$("#totalPriceFieldID").val(old_totalPrice);
							$("#netPayable").val(old_totalPrice);
							document.getElementById("row" + no + "").outerHTML = "";
							var table = document.getElementById("productTable");
							var table_len = (table.rows.length);
							//alert(table_len);
							  //document.getElementById("productTable").outerHTML = "";
							if(table_len==1){
								$("#tableHeaderID").hide();
								//$(".paymentButtonID").hide();
								//$("#emptyCartID").show();
								
							}
							
						} else {
							//jQuery("#quantity_errors").show();
							//jQuery("#quantity_errors").html(res.errors);
						}
					}
				}
			});
			
		}
	</script>
	<script type="text/javascript">
		function clearCart() {
			//event.preventDefault();
			//alert("hi");
			document.getElementById("myForm").reset();
			jQuery("#quantity_errors").hide();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "autocomplete/destroy",
				dataType: 'json',
				data: {
					//row_id: row_id,
					//quantity: quantity,
				},
				success: function(res) {
					if (res) {
						// Show Entered Value
						if (res.status === true) {
							//document.getElementById("row" + no + "").outerHTML = "";
							/*document.getElementById("productTable")
							.outerHTML = "";*/
							var table = document.getElementById("productTable");
							var table_len = (table.rows.length)-1;
							$("table tr").slice(-table_len).remove();
							$("#tableHeaderID").hide();
							$("#totalQuantityFieldID").val(0);
							//* for total price *//
							$("#totalPriceFieldID").val(0);
							$("#netPayable").val(0);
							//$(".paymentButtonID").hide();
							//$("#emptyCartID").show();
							//$("#totalDiscountInputVal").val(0);
							//$("#totalInputVal").val(0);
							//$(".totalAmountShow").html("Total Amount: 0 ?");
							
							
						} else {
							//jQuery("#quantity_errors").show();
							//jQuery("#quantity_errors").html(res.errors);
						}
					}
				}
			});			
		}
	</script>
	<script>
		$(document).ready(function(){
			$("#totalDiscount").change(function(event){
				//alert("The text has been changed.");
				var discount=$("#totalDiscount").val();
				var Tprice=$("#totalPriceFieldID").val();
				Tprice=(+Tprice)-(+discount);
				$("#netPayable").val(Tprice);
				
			});
		});
	</script>
	
	<script type="text/javascript">
		// incomeExpenseReport show
		$(document).ready(function() {
			$("#incomeExpenseReport").click(function(event) {
				alert("YEssssssss");
				event.preventDefault();
				var startDate = $("input#incomeStartDate").val();
				var startTime = $("input#incomeStartTime").val();
				var endDate = $("input#incomeEndDate").val();
				var endTime = $("input#incomeEndTime").val();
				//alert(productBarcode);
				alert("yes1");
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "autocomplete/ajax_incomeExpenseReport",
						dataType: 'json',
						data: {
							startDate: startDate,
							startTime: startTime,
							endDate: endDate,
							endTime: endTime
						},
						success: function(res) {
							if (res) {
								
								if (res.status === true) {
									alert("yes");
									
									
								}
								else {
									alert("NO");
									
								}
							}
						}
					});
			});
		});
	</script>
	
	<!--inventory table filter -->
	<script type="text/javascript">
			$(this).ready(function() {
				$('.filterable #filter_button').click(function(){
					var $panel = $(this).parents('.filterable'),
					$filters = $panel.find('.filtersField input'),
					$tbody = $panel.find('.table tbody');
					if ($filters.prop('disabled') == true) {
						$filters.prop('disabled', false);
						$filters.first().focus();
					} else {
						$filters.val('').prop('disabled', true);
						$tbody.find('.no-result').remove();
						$tbody.find('tr').show();
					}
				});
				
			});
	</script>
	<script>
			$('.filterable .filtersField input').keyup(function(e){
				//alert("hi");
					if($("#filterbarCode").val()|| $("#filterproductName").val()|| $("#filterproductGroup").val()||$("#filtersoldUnit").val()||$("#filterstockUnit").val()||$("#filtersupplierName").val()){
						
						var arr = [$("#filterbarCode").val(),$("#filterproductName").val(),$("#filterproductGroup").val(),$("#filtersoldUnit").val(),$("#filterstockUnit").val(),$("#filtersupplierName").val()];
						//arr[]
						//alert(arr.length);
							jQuery.ajax({
							//url: "http://[::1]/kenakata/autocomplete/lookup",  
							url: "<?php echo base_url(); ?>" + "manager/ajaxShowInventoryFilter",
							dataType: 'json',
							type: 'POST',
							data: {arr:arr},
							success: function(data) {
								if(data.status===true){
									$( ".remove_tr").remove();
									var len = data.infos.length;
									//alert(len);
									var txt = "";
									if(len > 0){
										for(var i=0;i<len;i++){
											if(data.infos[i].productBarcode && data.infos[i].productName){
												
												txt += "<tr class='remove_tr'><td>"+data.infos[i].productBarcode+"</td><td>"+data.infos[i].productName+"</td><td>"+data.infos[i].groupName+"</td><td>"+data.infos[i].productSaleCounter+"</td><td>"+data.infos[i].productQuantity+"</td><td>"+data.infos[i].supplierCompanyName+"</td><td>"+data.infos[i].productAddedDate+"</td><td><a href='<?php echo base_url('manager/productDetails/"+data.infos[i].productID+"'); ?>' type='button' class='btn btn-sm btn-primary' data-toggle='tooltip' data-placement='top' title='Details'><i class='fa fa-info'></i></a></td></tr>";
												//alert(txt);
											}
										}
										if(txt != ""){
											$("#showInventorytable").append(txt);
										}
									}
								}
							},
						});
					}
					else{
						$( ".remove_tr").remove();
					}
			});
	</script>
	
	<script type="text/javascript">
		$('.modal').on('shown.bs.modal', function() {
			$('.confrimOrderButton').focus();
		});
		$('.modal').on('shown.bs.modal', function() {
			$('.returnConfirmButton').focus();
		});
		
		$('.keyEnter').keypress(function(event){
			if(event.keyCode == 13){
				$('.confirmOrder').click();
				return false;
			}
		});
		$('.keyEnter').keypress(function(event){
			if(event.keyCode == 13){
				$('.returnButton').click();
				return false;
			}
		});
	</script>
	
	<script>
		$(document).ready(function(){
			$(".returnProductID").change(function(event){
				//alert("The text has been changed.");
				var product_id = $(".returnProductID").val();
				if($("#returnProductQuantityID").val()){
				var product_quantity = $("#returnProductQuantityID").val();
				}
				else{
				 var product_quantity=1;
				}
				//alert(product_id);
				jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + "manager/ajax_product",
                    dataType: 'json',
                    data: {
                       product_id:product_id
                    },
                    success: function(res) {
					if (res) {
                            if (res.status === true){
								//alert("YES");
								$("#returnProductPriceID").val(res.productSalePrice);
								$("#returnProductQuantityID").val(product_quantity);
								var oldCalc =($("#returnProductPriceID").val())*($("#returnProductQuantityID").val());
								var newCalc =($("#returnNewProductPriceID").val())*($("#returnNewProductQuantityID").val());
								var total=newCalc-oldCalc;
								if(total>0){
									$("#returnProductCalc").html("Customer has to Pay: "+total);
								}
								else{
									total=(-1)*(total);
									$("#returnProductCalc").html("You have to Pay: "+total);
								}
							}
                            else {
								$("#returnProductPriceID").val(00);
                            }
                        }
                    }
                });
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$(".returnNewProductID").change(function(event){
				//alert("The text has been changed.");
				var product_id = $(".returnNewProductID").val();
				if($("#returnNewProductQuantityID").val()){
				var product_quantity = $("#returnNewProductQuantityID").val();
				}
				else{
				 var product_quantity=1;
				}
				//alert(product_id);
				jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + "manager/ajax_product",
                    dataType: 'json',
                    data: {
                       product_id:product_id
                    },
                    success: function(res) {
					if (res) {
                            if (res.status === true){
								
								$("#returnNewProductPriceID").val(res.productSalePrice);
								$("#returnNewProductQuantityID").val(product_quantity);
								var oldCalc =($("#returnProductPriceID").val())*($("#returnProductQuantityID").val());
								var newCalc =res.productSalePrice*product_quantity;
								var total=newCalc-oldCalc;
								if(total>0){
									$("#returnProductCalc").html("Customer has to Pay: "+total);
								}
								else{
									$("#returnProductCalc").html("You have to Pay: "+total);
								}
							}
                            else {
								total=(-1)*(total);
								$("#returnNewProductPriceID").val(0);
                            }
                        }
                    }
                });
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("#returnProductQuantityID").change(function(event){
				//alert("The text has been changed.");
				var oldCalc =($("#returnProductPriceID").val())*($("#returnProductQuantityID").val());
				var newCalc =($("#returnNewProductPriceID").val())*($("#returnNewProductQuantityID").val());
				var total=newCalc-oldCalc;
				if(total>0){
					$("#returnProductCalc").html("Customer has to Pay: "+total);
				}
				else{
					total=(-1)*(total);
					$("#returnProductCalc").html("You have to Pay: "+total);
				}
			});
		});
	</script>
	<script>
		$(function(){
			$("#returnNewProductQuantityID").change(function(event){
				//alert("The text has been changed.");
				var oldCalc =($("#returnProductPriceID").val())*($("#returnProductQuantityID").val());
				var newCalc =($("#returnNewProductPriceID").val())*($("#returnNewProductQuantityID").val());
				var total=newCalc-oldCalc;
				if(total>0){
					$("#returnProductCalc").html("Customer has to Pay: "+total);
				}
				else{
					total=(-1)*(total);
					$("#returnProductCalc").html("You have to Pay: "+total);
				}
			});
		});
	</script>
	
	<script>
		Mousetrap.bind("enter", function(e) {
			$(".returnConfirmButton").click();
		});
	</script>
	 <script>
                     //$this->load->library('cart');
                     var count=1;
                    // $(this).attr('id'+count );
                     var idCount = $("#rowid").val();

                     $(document).ready(function(){
                        $("#addCart").click(function(){
                        	var id1=$("#client").val();
                        	var id2=$("#productId").val();
                        	var id3=$("#productQuantity").val();

                        			if(id1 && id2 && id3){
                        				count++;
                        				$("#id1").append(id1+"<br>");
                        				$("#id2").append(id2+"<br>");
		                        		$("#id3").append(id3+"<br>");
		                        		$("#id4").append("<span class='glyphicon glyphicon-remove-sign btn-danger'></span><br>");
		                        	}
                        			else alert("Please Enter Quantity/Select Product/Select Manufacturer");
                        });
                         $("#id4").click(function(){
                        	alert("We are trying to remove it"+this.id);
                        	//alert($(this).closest('tr').index());
                        	var val;
	  						<?php $cart=$this->cart->contents(); ?>
	                        $('#cartData tr').each(function() {
						        alert(idCount);
						        $(".rowid").remove(idCount);
						        idCount++;
						       
		                    });

					    });
                        $('button #addCart').each(function() {
						   $(this).attr('id', 'q' + idCount);
						   idCount++;
						});
						
                     });
                     
                     
					
    </script>
</body>

</html>
