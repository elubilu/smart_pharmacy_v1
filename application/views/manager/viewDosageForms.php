<?php include('header.php') ?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">View Dosage Forms</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Dosage Forms</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
<?php echo form_open('manager/updateDosageForms'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Dosage Forms</label>
                        <input type="text" class="form-control removeDisabled" name="doseForm" value="<?php echo $data->doseForm ?>" disabled>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Note</label>
                        <textarea name="dosageNote" class="form-control removeDisabled" value="" disabled><?php echo $data->dosageNote ?></textarea>
                     </div>
                  </div>                 
               </div>  
               <input type="hidden" class="form-control" name="doseId" value="<?php echo $data->doseId ?>" >
               <div class="row">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-primary" id="removeDisabledButton"><i class="fa fa-pencil"></i> Edit</button>
                     <button type="submit" class="btn btn-success addDisabled hidden"><i class="fa fa-thumbs-up"></i> Save</button>
                     <button type="button" class="btn btn-warning addDisabled hidden" id="addDisabledButton"><i class="fa fa-times"></i> Cancel</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div> 
   <?php echo form_close(); ?>
<?php include('footer.php') ?>