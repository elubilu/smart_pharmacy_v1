<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Add Product to Stock</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add Product to Stock</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
   <?php $count=1; ?>
  
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Select Manufacturer / Supplier</label>
                        <select name="clientType" id="client" class="form-control forselect2" required>
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($manufacturers as $values) {
                              
                           ?>
                           <option value="<?php echo $values->manufacturerCompanyName ?>"><?php echo $values->manufacturerCompanyName ?></option>             
                           <?php } ?>          
                        </select>
                         <div class="errorClass"><?php echo form_error('clientType'); ?></div>
                     </div>
                  </div>
               
   				
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Product</label>
                        <select name="productId" id="productId" class="form-control" required>
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($data as $value) {
                              
                           ?>
                           <option value="<?php echo $value->productName ?>"><?php echo $value->productName ?></option>             
                           <?php } ?>             
                        </select>
                        <div class="errorClass"><?php echo form_error('productId'); ?></div>
                     </div>
                   </div>
                  </div>   
                  <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Quantity</label>
                        <input type="text" class="form-control" name="productQuantity" id="productQuantity" required>
                         <div class="errorClass"><?php echo form_error('productQuantity'); ?></div>
                     </div>
                  </div>
                 <input type="hidden" id="rowid" value="<?php  echo $count; ?>" name="rowid" class="" /> 
               <div class="row">                  
                  <div class="col-md-12">
                     <button type="submit" id="addCart" name="addCart"class="btn btn-primary">Add</button>
                     <a type="button" onClick="clearCart()"  class="btn btn-danger">Clear Cart</a>
                  </div>
               </div>
               <?php //$this->cart->insert($data);?>
                
           <?php echo form_open('manager/storeProductToStock'); ?>

               <div class="row m-top-25 m-bottom-25">
                  <div class="col-md-12">
   				
   					<table class="table table-striped">
   						<thead>
   							<tr id="tableHeaderID">
                           <th>Manufacturer </th>
   								<th>Product </th>
   								<th>Quantity </th>
   								<th>Remove</th>
   							</tr>
   						</thead>
   						<tbody id="cartData">
                        <?php //if($cart=$this->cart->contents()){ ?>
                           <?php //foreach($cart as $item): ?>
   								<tr>
                              <td id="id1"></td>
                              <td id="id2"></td>
                              <td id="id3"></td>
                              <td id="id4"></td>
   								</tr>
                           <?php //echo form_hidden('rowid'); ?>
                           <?php ?>
   						</tbody>
   					</table>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Done</button>
                  </div>
               </div>  
            </div>
         </div>
      </div>      
   </div>
   <?php echo form_close(); ?>
<?php include('footer.php') ?>