<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Smart Pahrmacy v1.0</title>

    <!-- Custom Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-timepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_table.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
	
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url('manager/');?>">Store Name</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="<?php  echo base_url('manager/myAccount'); ?>"><i class="fa fa-user fa-fw"></i> My Account</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php  echo base_url('login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="text-center user-img hidden-xs hidden-sm">
                            <img src="http://localhost/mama_project/assets/images/forhad mama1.jpg" alt="" class="img-responsive img-circle"> <br> User: Riaz Ahmed
                        </li>-->
                        <li>
                            <a href="<?php echo base_url('manager/index'); ?>"><i class="fa fa-bar-chart fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Invoicing<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('manager/createInvoice'); ?>"> Create Invoice</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manager/returnExchange'); ?>">Return / Exchange</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Inventory<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('manager/addProductToStock'); ?>"> Add Product to Stock</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manager/products'); ?>"> Products</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manager/manufacturersSuppliers'); ?>"> Manufacturers & Suppliers</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manager/medicineGroup'); ?>"> Medicine Group</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('manager/dosageForms'); ?>"> Dosage Forms</a>
                                </li>
                            </ul>
                        </li>                        
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('manager/productReport'); ?>"> Product Report</a>
                                    <a href="<?php echo base_url('manager/saleReport'); ?>"> Sale Report</a>
                                    <a href="<?php echo base_url('manager/stockReport'); ?>"> Stock Report</a>
                                    <a href="<?php echo base_url('manager/stockEntryReport'); ?>"> Stock Entry Report</a>
                                    <a href="<?php echo base_url('manager/log'); ?>"> Log</a>
                                </li>
                            </ul>
                        </li>                    
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> Other<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('manager/license'); ?>"> License</a>
                                    <a href="<?php echo base_url('manager/allUser'); ?>">All User</a>
                                    <a href="<?php echo base_url('manager/discountRate'); ?>">Discount Rate</a>
                                    <a href="<?php echo base_url('manager/returnPercentageRate'); ?>">Return Percentage Rate</a>
                                    <a href="<?php echo base_url('manager/invoiceSettings'); ?>"> Invoice Settings</a>
                                </li>
                            </ul>
                        </li>                
						<li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> StarLab Admin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('manager/incomeExpenseReport'); ?>"> Income-Expense Report</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">