<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">View Manufacturer</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Manufacturer</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
<?php echo form_open('manager/updateManufacturer'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control removeDisabled" name="manufacturerCompanyName" value="<?php echo $data->manufacturerCompanyName ?>" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Representative</label>
                        <input type="text" class="form-control removeDisabled" name="manufacturerRepresentative" value="<?php echo $data->manufacturerRepresentative?>" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Representative Contact</label>
                        <input type="text" class="form-control removeDisabled" name="manufacturerRepresentativeContact" value="<?php echo $data->manufacturerRepresentativeContact?>" disabled>
                     </div>
                  </div>                  
               </div>  
                <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Company Address</label>
                        <textarea name="manufacturerAddress" id="" rows="3" class="form-control removeDisabled" disabled><?php echo $data->manufacturerAddress?></textarea>
                        <div class="errorClass"><?php echo form_error('manufacturerAddress'); ?></div>
                     </div>
                  </div>                 
               </div> 
                <input type="hidden" class="form-control removeDisabled" name="manufacturerId" value="<?php echo $data->manufacturerId ?>" >
               <div class="row">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-primary" id="removeDisabledButton"><i class="fa fa-pencil"></i> Edit</button>
                     <button type="submit" class="btn btn-success addDisabled hidden"><i class="fa fa-thumbs-up"></i> Save</button>
                     <button type="button" class="btn btn-warning addDisabled hidden" id="addDisabledButton"><i class="fa fa-times"></i> Cancel</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div>
<?php echo form_close(); ?>
<?php include('footer.php') ?>