<?php include('header.php') ?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Add New Dosage Forms</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add New Dosage Forms</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
   <?php echo form_open('manager/storeDosageForms'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Dosage Forms</label>
                        <input type="text" class="form-control" name="doseForm" value="">
                        <div class="errorClass"><?php echo form_error('doseForm'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Note</label>
                        <textarea name="dosageNote" class="form-control"></textarea>
                        <div class="errorClass"><?php echo form_error('dosageNote'); ?></div>
                     </div>
                  </div>                 
               </div>  

               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Add</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div> 
   <?php echo form_close(); ?>

<?php include('footer.php') ?>