<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Manufacturers / Suppliers</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Manufacturers / Suppliers</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>

   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info filterable">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-md-6">
                     <h4>Searched Product</h4>
                  </div>
                  <div class="col-md-6">
                     <div class="pull-right p-top-20">
                        <a href="<?= base_url('manager/addNewManufacturer')?>" class="btn btn-warning"> Add New Manufacturer</a>
                        <button id="filter_button" class="btn btn-warning btn-filter with_print" ><i class="fa fa-filter"></i> Filter
                        </button>
                     </div>                     
                  </div>
               </div>
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <table class="table table-striped">
                        <thead>
                           <tr class="active filters">
                              <th>
                                 <input type="text" class="form-control" placeholder="ID" disabled data-toggle="true" id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Manufacturer" disabled id="">
                              </th>
                              <th>
                                 <span >View</span>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($data as  $value) {
                             
                           ?>
                           <tr>
                              <td><?php echo $value->manufacturerId; ?></td>
                              <td><?php echo $value->manufacturerCompanyName; ?></td>
                              
                              <td><a href="<?php echo base_url("manager/viewManufacturer/{$value->manufacturerId}")?>" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></a></td>
                           </tr> 
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>                  
            </div>   
         </div>   
      </div>
   </div> 
<?php include('footer.php') ?>