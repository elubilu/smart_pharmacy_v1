<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Create Invoice</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Create Invoice</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>

   <div class="row">
      <div class="col-md-6">
         <div class="panel panel-info">
            <div class="panel-body">	
   				<div class="row">
   					<div class="col-md-6">
   						<div class="form-group">
   							<label>Product</label>
   							<input type="text" class="form-control" name="" id="">
   						</div>
   					</div>
   					<div class="col-md-6">
   					  <div class="form-group">
   						 <label>Quantity</label>
   						 <input type="number" min="1" class="form-control" name="quantity" id="quantity">
   						 <div style="" id="quantity_errors" class="warningSize red-text"></div>
   					  </div>
   					</div>
   				</div>
               <div class="row">                  
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary submit" style="">Add</button>
                    <a type="button" onClick="clearCart()"  class="btn btn-danger">Clear Cart</a>
                  </div>
               </div>
               <div class="row m-top-25 m-bottom-25">
                  <div class="col-md-12">
   				
   					<table class="table table-striped">
   						<thead>
   							<tr id="tableHeaderID">
   								<th >Product </th>
   								<th >Quantity </th>
   								<th> Price</th>
   								<th>Remove</th>
   							</tr>
   						</thead>
   						<tbody>
   								<tr>
                              <td> Napa Extra</td>
                              <td> 10</td>
                              <td> 2.5</td>
   									<td> 
                                 <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-times"></i>
                                 </button>
                              </td>
   								</tr>
   						</tbody>
   					</table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Search Product by Group</label>
                        <select name="" id="" class="form-control forselect2">
                           <option value="0">Select an Option</option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row m-top-25 m-bottom-25">
                  <div class="col-md-12">
               
                  <table class="table table-striped">
                     <thead>
                        <tr id="tableHeaderID">
                           <th >Brand Name </th>
                           <th >Price </th>
                           <th> Available Quantity</th>
                           <th>Add</th>
                        </tr>
                     </thead>
                     <tbody>
                           <tr>
                              <td> Napa Extra</td>
                              <td> 10</td>
                              <td> 2.5</td>
                              <td> 
                                 <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i>
                                 </button>
                              </td>
                           </tr>
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Subtotal</label>
                        <input class="form-control" id="" value="" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Discount (%)</label>
                        <input class="form-control" id="" value="" >
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Additional Discount</label>
                        <input class="form-control" id="" value="" disabled>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Grand Total</label>
                        <input class="form-control" id="" value="" name="" disabled>
                     </div>
                  </div>
      				<div class="col-md-6">
      					<div class="form-group">
      						<label>Customer Savings</label>
      						<input class="form-control" id="" value="" name="" disabled>
      					</div>
      				</div>
               </div>            
               <div class="row">
                  <div class="col-md-12">
                     <a type="submit" class="btn btn-primary">Done</a>
                  </div>
               </div>       
            </div>
         </div>
      </div>
   </div> 
<?php include('footer.php') ?>