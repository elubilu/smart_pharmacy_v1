<?php include('header.php') ?>
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Dosage Forms</h3>
      </div>
   </div>

	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Dosage Forms</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>

   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info filterable">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-md-12">
                     <div class="pull-right p-top-20">
                        <a href="<?= base_url('manager/addNewDosageForms')?>" class="btn btn-warning"> Add New Dosage Form</a>
                        <button id="filter_button" class="btn btn-warning btn-filter with_print" ><i class="fa fa-filter"></i> Filter
                        </button>
                     </div>                     
                  </div>
               </div>
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <table class="table table-striped">
                        <thead>
                           <tr class="active filters">
                              <th>
                                 <input type="text" class="form-control" placeholder="ID" disabled data-toggle="true" id="">
                              </th>
                              <th>
                                 <input type="text" class="form-control" placeholder="Dosage Forms" disabled id="">
                              </th>
                              <th>
                                 <span >View</span>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($data as  $value) {
                            ?>
                           <tr>
                              <td><?php echo $value->doseId; ?></td>
                              <td><?php echo $value->doseForm; ?></td>
                              <td><a href="<?php echo  base_url("manager/viewDosageForms/{$value->doseId}")?>" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></a></td>
                           </tr> 
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>                  
            </div>   
         </div>   
      </div>
   </div> 
<?php include('footer.php') ?>