<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Dashboard</h3>
		</div>		
	</div>
	
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li class="active">Dash Board</li>
         </ol>
      </div>
   	</div>
   	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-xs-12 col-md-4 col-sm-4 col-lg-3 dashboard-total-cash">
					<div class="panel panel-info">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 ">
									<i class="fa fa-money" aria-hidden="true"></i>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
									<h4>Total Cash </h4>
									<h4>৳ <?php// echo $cash; ?></h4>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>