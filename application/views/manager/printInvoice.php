<html>
    <head>
        <title>POS PRINT</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <style>
            /* cyrillic-ext */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37ZjTOQ_MqJVwkKsUn0wKzc2I.woff2) format('woff2');
              unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
            }
            /* cyrillic */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'),
               url(../fonts/y7lebkjgREBJK96VQi37ZjUj_cnvWIuuBMVgbX098Mw.woff2) format('woff2');
              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37ZkbcKLIaa1LC45dFaAfauRA.woff2) format('woff2');
              unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37Zmo_sUJ8uO4YLWRInS22T3Y.woff2) format('woff2');
              unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37Zr6up8jxqWt8HVA3mDhkV_0.woff2) format('woff2');
              unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37ZiYE0-AqJ3nfInTTiDXDjU4.woff2) format('woff2');
              unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
              font-family: 'Roboto Slab';
              font-style: normal;
              font-weight: 400;
              src: local('Roboto Slab Regular'), local('RobotoSlab-Regular'), 
              url(../fonts/y7lebkjgREBJK96VQi37Zo4P5ICox8Kq3LLUNMylGO4.woff2) format('woff2');
              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
            }
            body{
                font-size:9px;
            }
            .wrapper{
                margin:0px auto;
                width:90mm;
                ~background-color: #EEE; 
                font-family: 'Roboto Slab', serif;
                border:1px solid #DDD;
            }
            hr{
                margin-bottom:0px;
            }
            .printed-area{
                margin:0mm 2mm;
            }
            .print-header{
                text-align: center;
            }
            .print-header h1{
                font-weight: 600;
                margin-bottom: -15px;
            }
            .print-header p{
                font-weight: 100;
                font-size:12px;
            }
            table{
                width:100%;
                text-align: left;
                font-size: 12px;
            }
            table thead th{
                border-bottom:1px dashed #333;
            }
            table.cart-table td{
                ~border-bottom:1px dashed #333;
            }
            table.cart-table tbody tr:nth-child(odd){
                ~background-color: #EEE; 
            }
            tfoot{
                text-align: right;
                font-weight:100;
            }
            button:hover{
                cursor: pointer;                
            }
            .btns{
                margin:0px auto;
                width:90mm;
                margin-top: 25px;
                text-align: center;
            }
            .btn-print{
                background-color: orange;
                color: #FFF; 
                font-size: 16px;
                padding: 10px 15px;
                border-radius: 4px;
                border: 1px solid orange;
                text-decoration: none;
            }
            .btn-danger{
                background-color: #CD3131;
                color: #FFF; 
                font-size: 16px;
                padding: 10px 15px;
                border-radius: 4px;
                border: 1px solid #CD3131;
                text-decoration: none;
            }
            @media print{
                body{
                    font-size:9px;
                }
                .no_print{
                    display: none;
                }
                .wrapper{                   
                    font-family: 'Roboto Slab', serif;
                    border:0px solid #DDD;
                }
                hr{
                    margin-bottom:0px;
                }
                .printed-area{
                    margin:0mm 2mm;
                }
                table thead th{
                    border-bottom:1px dashed #333;
                }
                table.cart-table td{
                    border-bottom:1px dashed #333;
                }
            }
        </style>
    </head>

    <body>
        <div class="wrapper">
            <div class="printed-area">
                <div class="print-header">
                    <h1>Excellent Shoe</h1>
                    <p>
                        <?php echo $info->shopTitle; ?> <br>
                        Zindabazar, Sylhet 
                    </p>
                </div>
                <div class="print-body">
                    <table class="sale-info">
                        <tbody>
                            <tr>
                                <td>Invoice No.: <?php echo $info->saleID; ?></td>
                                <td style="text-align: right;">Date: <?php echo $info->saleDate; ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if($info->salesman_info_salesmanID>0){  ?>
                                    Served By : <?php echo $info->salesmanName; ?>
                                    <?php } ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <table class="cart-table">
                        <thead>
                            <tr>
                                <th style="width:45%;">Product Name</th>
                                <th style="width:10%;text-align: right;">Rate</th>
                                <th style="width:15%;text-align: right;">Qty</th>
                                <th style="width:25%;text-align: right;">Total</th>
                            </tr>
                        </thead>
                        <?php $totalqty=0; $totalAmount=0;?>
                        <tbody>
                            <?php foreach($details as $infod):?>
                            <tr>
                                <td style="width:45%;"><?php echo $infod->productName; ?></td>
                                <td style="width:10%;text-align: right;"><?php echo $infod->salePrice; ?></td>
                                <td style="width:15%;text-align: right;"><?php echo $infod->saleProductQuantity; ?></td>
                                <td style="width:25%;text-align: right;"><?php echo ($infod->saleProductQuantity*$infod->salePrice); ?></td>
                            </tr>   
                            <?php $totalqty=$totalqty+$infod->saleProductQuantity; ?>
                            <?php //$totalAmount=$totalAmount+($info->saleProductQuantity*$info->salePrice); ?>
                            <?php endforeach; ?>                        
                        </tbody>
                    </table>
                    <table style="text-align: right;">
                        <tbody>
                            <tr>
                                <th colspan="2" style="width:40%"></th>
                                <th style="width:35%">Total Items</th>
                                <td style="width:25%"><?php echo $totalqty; ?></td>
                            </tr>
                            <tr>
                                <th colspan="2"></th>
                                <th>Total</th>
                                <td><?php echo $info->saleTotalAmount; ?></td>
                            </tr>
                            <tr>
                                <th colspan="2"></th>
                                <th>Grand Total</th>
                                <td><?php echo $info->saleTotalAmount; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="print-footer">
                    <p style="text-align:center;padding-bottom:5px;border-bottom: 1px solid #000;">
                        ☺ Thank you for your business ☺
                    </p>
                    <p style="text-align:center;">Developed By - STARLAB IT (01719450855) <br>
                    www.starlabit.com.bd
                    </p>
                </div>              
            </div>
        </div>
        <div class="btns no_print">
            <button type="button" class="btn-print" onclick="window.print();"> Print</button>
            <a type="button" href="<?php echo base_url("manager/addBillMemo"); ?>" class="btn-danger" > Back</a>
        </div>


        

        <script type="text/javascript">       
            $(function(){
                $(window).on("load", function(){
                  //alert("hi");
                  window.print();
                });
            });
        </script>
    </body>

</html>