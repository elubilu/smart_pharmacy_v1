<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add User</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add User</li>
         </ol>
      </div>
   	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<?php //print_r($info); ?>
	<?php include('messages.php');?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Full Name </label>
								<input type="text" class="form-control" name="adminName" value=""/>
								<div class="errorClass"><?php echo form_error('adminName'); ?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Contact Number</label>
								<input type="text" class="form-control" name="adminContact" value="" readonly />
								<div class="errorClass"><?php echo form_error('adminContact'); ?></div>
							</div>							
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" name="adminEmail" value="" readonly  />
								<div class="errorClass"><?php echo form_error('adminEmail'); ?></div>
							</div>							
						</div>						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label> Password</label>
								<input type="password" class="form-control" name="adminPassword" value="" />
								<div class="errorClass"><?php echo form_error('adminPassword'); ?></div>
							</div>	
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Retype Password</label>
								<input type="password" class="form-control" name="confirm_password" value="" />
								<div class="errorClass"><?php echo form_error('confirm_password'); ?></div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>User Address</label>
								<textarea name="adminAddress" class="form-control"></textarea>
								<div class="errorClass"><?php echo form_error('adminAddress'); ?></div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary"> Add user</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>