<?php include('header.php') ?>
<?php 
      //$totalProductQty=0;
      //$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Add New Manufacturer</h3>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add New Manufacturer</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
<?php echo form_open('manager/storeNewManufacturer'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" name="manufacturerCompanyName" value="">
                        <div class="errorClass"><?php echo form_error('manufacturerCompanyName'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Representative</label>
                        <input type="text" class="form-control" name="manufacturerRepresentative" value="">
                        <div class="errorClass"><?php echo form_error('manufacturerRepresentative'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Representative Contact</label>
                        <input type="text" class="form-control" name="manufacturerRepresentativeContact" value="">
                        <div class="errorClass"><?php echo form_error('manufacturerRepresentativeContact'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Company Address</label>
                        <textarea name="manufacturerAddress" id="" rows="3" class="form-control" ></textarea>
                        <div class="errorClass"><?php echo form_error('manufacturerAddress'); ?></div>
                     </div>
                  </div>                 
               </div>  
               
               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Add</button>
                  </div>
               </div>       
            </div>
         </div>
      </div>      
   </div>
<?php echo form_close(); ?>
<?php include('footer.php') ?>