<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">View Medicine Group</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Medicine Group</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
<?php echo form_open('manager/updateMedicineGroup'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Group Name</label>
                        <input type="text" class="form-control removeDisabled" name="medicineGroupName" value="<?php echo $data->medicineGroupName ?>" disabled>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Contains</label>
                        <input type="text" class="form-control removeDisabled" name="medicineGroupContains" value="<?php echo $data->medicineGroupContains ?>" disabled>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Note</label>
                        <textarea name="medicineGroupNote" class="form-control removeDisabled" value="" disabled><?php echo $data->medicineGroupNote ?></textarea>
                     </div>
                  </div>                 
               </div>  
               <input type="hidden" class="form-control removeDisabled" name="medicineGroupId" value="<?php echo $data->medicineGroupId ?>" >
               <div class="row">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-primary" id="removeDisabledButton"><i class="fa fa-pencil"></i> Edit</button>
                     <button type="submit" class="btn btn-success addDisabled hidden"><i class="fa fa-thumbs-up"></i> Save</button>
                     <button type="button" class="btn btn-warning addDisabled hidden" id="addDisabledButton"><i class="fa fa-times"></i> Cancel</button>
                  </div>
               </div> 
                    	 
            </div>
         </div>
      </div>      
   </div> 
   <?php echo form_close(); ?>
<?php include('footer.php') ?>