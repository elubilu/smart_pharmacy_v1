<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">Add New Product</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">Add New Product</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
   <?php echo form_open('manager/storeProduct'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Barcode</label>
                        <input type="text" class="form-control" name="productBarcode" value="">
                        <div class="errorClass"><?php echo form_error('productBarcode'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Brand Name*/ Product Name</label>
                        <input type="text" class="form-control" name="productName" value="">
                        <div class="errorClass"><?php echo form_error('productName'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Group</label>
                        <select name="group_info_productGroupId" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($groups as $group) {
                              # code...
                           ?>
                           <option value="<?php echo $group->medicineGroupId; ?>"><?php echo $group->medicineGroupName; ?></option>             
                           <?php } ?>           
                        </select>
                        <! <input type="text" class="form-control" name="" value="">
                        <div class="errorClass"><?php echo form_error('group_info_productGroupId'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Contains</label>
                        <input type="text" class="form-control" name="productContains" value="">
                        <div class="errorClass"><?php echo form_error('productContains'); ?></div>
                     </div>
                  </div>                 
               </div>  
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Dosage Form</label>
                        <select name="dosage_info_productDosageFormId" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($dosage as $value1) {
                              # code...
                           ?>
                           <option value="<?php echo $value1->doseId; ?>"><?php echo $value1->doseForm; ?></option>             
                           <?php } ?>           
                        </select>
                        <!<input type="text" class="form-control" name="dosage_info_productDosageFormId" value="">
                        <div class="errorClass"><?php echo form_error('dosage_info_productDosageFormId'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Manufacturer</label>
                         <select name="manufacturer_info_productManufacturerId" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($manufacturers as $value) {
                              # code...
                           ?>
                           <option value="<?php echo $value->manufacturerId; ?>"><?php echo $value->manufacturerCompanyName; ?></option>             
                           <?php } ?>           
                        </select>
                        <!<input type="text" class="form-control" name="manufacturer_info_productManufacturerId" value="">
                        <div class="errorClass"><?php echo form_error('manufacturer_info_productManufacturerId'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Purchase price</label>
                        <input type="text" class="form-control" name="productPurchasePrice" value="">
                        <div class="errorClass"><?php echo form_error('productPurchasePrice'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>MRP</label>
                        <input type="text" class="form-control" name="productMRP" value="">
                        <div class="errorClass"><?php echo form_error('productMRP'); ?></div>
                     </div>
                  </div>
                <!--  <div class="col-md-4">
                     <div class="form-group">
                        <label>Stock Quantity</label>
                        <input type="text" class="form-control" name="productQuantity" value="">
                        <div class="errorClass"><?php echo form_error('productQuantity'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Sold Quantity</label>
                        <input type="text" class="form-control" name="productSaleCounter" value="">
                        <div class="errorClass"><?php echo form_error('productSaleCounter'); ?></div>
                     </div>
                  </div>                  
               </div>  
-->
               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" class="btn btn-primary">Add</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div> 
<?php echo form_close(); ?>

<?php include('footer.php') ?>