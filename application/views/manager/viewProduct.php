<?php include('header.php') ?>
<?php 
		//$totalProductQty=0;
		//$totalProductPrice=0;
?> 
   <div class="row">
      <div class="col-md-12">
         <h3 class="page-header">View Product</h3>
      </div>
   </div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Dash Board</a> </li>
            <li class="active">View Product</li>
         </ol>
      </div>
   </div>

   <?php include('successErrorMessage.php') ?>
 <?php echo form_open('manager/updateProduct'); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-body">   
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Barcode</label>
                        <input type="text" class="form-control" name="productBarcode" value="<?php echo $data->productBarcode ?>" disabled>
                        <div class="errorClass"><?php echo form_error('productBarcode'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Brand Name*/ Product Name</label>
                        <input type="text" class="form-control removeDisabled" name="productName" value="<?php echo $data->productName ?>" disabled>
                        <div class="errorClass"><?php echo form_error('productName'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Group</label>
                         <select name="group_info_productGroupId" id="" class="form-control forselect2">
                           <option value="0" selected readonly> Select an Option</option> 
                           <?php foreach ($groups as $group) {
                              # code...
                           ?>
                           <option value="<?php echo $group->medicineGroupId; ?>"><?php echo $group->medicineGroupName; ?></option>             
                           <?php } ?>           
                        </select>
                       
                        <div class="errorClass"><?php echo form_error('group_info_productGroupId'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Contains</label>
                        <input type="text" class="form-control removeDisabled" name="productContains" value="<?php echo $data->productContains ?>" disabled>
                     <div class="errorClass"><?php echo form_error('productContains'); ?></div>
                     </div>
                  </div>                 
               </div>  
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Dosage Form</label>
                        <input type="text" class="form-control removeDisabled" name="dosage_info_productDosageFormId" value="<?php echo $data->dosage_info_productDosageFormId ?>" disabled>
                        <div class="errorClass"><?php echo form_error('dosage_info_productDosageFormId'); ?></div>                 
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Manufacturer</label>
                        <select name="manufacturer_info_productManufacturerId" id="" class="form-control forselect2">
                           <option value="<?php echo $data->manufacturer_info_productManufacturerId ?>" selected readonly> Select an Option</option> 
                           <?php foreach ($manufacturers as $value) {
                              # code...
                           ?>
                           <option value="<?php echo $value->manufacturerId; ?>"><?php echo $value->manufacturerCompanyName; ?></option>             
                           <?php } ?>           
                        </select>
                        <div class="errorClass"><?php echo form_error('manufacturer_info_productManufacturerId'); ?></div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Purchase price</label>
                        <input type="text" class="form-control removeDisabled" name="productPurchasePrice" value="<?php echo $data->productPurchasePrice ?>" disabled>
                        <div class="errorClass"><?php echo form_error('productPurchasePrice'); ?></div>
                     </div>
                  </div>                  
               </div>  
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>MRP</label>
                        <input type="text" class="form-control removeDisabled" name="productMRP" value="<?php echo $data->productMRP ?>" disabled>
                        <div class="errorClass"><?php echo form_error('productMRP'); ?></div>
                     </div>
                  </div>
      <!--
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Stock Quantity</label>
                        <input type="text" class="form-control removeDisabled" name="" value="" disabled>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Sold Quantity</label>
                        <input type="text" class="form-control removeDisabled" name="" value="" disabled>
                     </div>
                  </div>                  
               </div>  
       -->
       <input type="hidden" class="form-control" name="productId" value="<?php echo $data->productId ?>" >
               <div class="row">
                  <div class="col-md-12">
                     <button type="button" class="btn btn-primary" id="removeDisabledButton"><i class="fa fa-pencil"></i> Edit</button>
                     <button type="submit" class="btn btn-success addDisabled hidden"><i class="fa fa-thumbs-up"></i> Save</button>
                     <button type="button" class="btn btn-warning addDisabled hidden" id="addDisabledButton"><i class="fa fa-times"></i> Cancel</button>
                  </div>
               </div>   	 
            </div>
         </div>
      </div>      
   </div>
<?php echo form_close(); ?>
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-info filterable">
            <div class="panel-heading">
               <div class="row">
                  <div class="col-md-6">
                     <h4>Searched Sale Info</h4>
                  </div>
                  <div class="col-md-6">
                     <div class="pull-right p-top-20">
                        <button id="filter_button" class="btn btn-warning btn-filter with_print" ><i class="fa fa-filter"></i> Filter
                        </button>
                     </div>                     
                  </div>
               </div>
            </div>
            <div class="panel-body card-shadow">
               <div class="row">
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>From Date</label>
                              <input type="text" class="form-control" id="startDate" name="" value="">
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>To Date</label>
                              <input type="text" class="form-control" id="endDate" name="" value="">
                           </div>
                        </div>     
                     </div>
                     <div class="row">                         
                        <div class="col-md-12">
                           <button class="btn btn-primary" type="submit"> Search</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <table class="table table-striped">
                        <thead>
                           <tr>
                              <th>Date-Time</th>
                              <th>Bill No.</th>
                              <th>Quantity</th>
                              <th>Sold by (Salesman)</th>
                              <th>Total Amount</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>12-10-2017, 15:18:23</td>
                              <td>106597</td>
                              <td>10</td>
                              <td>Kuddus</td>
                              <td>5264</td>
                           </tr> 
                        </tbody>
                     </table>
                  </div>
               </div>                  
            </div>   
         </div>   
      </div>
   </div> 
<?php include('footer.php') ?>