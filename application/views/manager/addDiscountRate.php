<?php include('header.php') ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Discount Rate</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('manager/');?>">Add Discount Rate</a> </li>
            <li class="active">Add User</li>
         </ol>
      </div>
   	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if($this->session->flashdata('feedback_successfull'))
					{ ?>
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="fa fa-times"></i></span>
								</button>
							<strong>Success!</strong>
							<?php echo $this->session->flashdata('feedback_successfull'); ?>
						</div>
					<?php } 
					if($this->session->flashdata('feedback_failed'))
						{ ?>
							<div class="alert alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="fa fa-times"></i></span>
									</button>
								<strong>Oops!</strong>
								<?php echo $this->session->flashdata('feedback_failed'); ?>
							</div>
				<?php   } ?>
		</div>
	</div>
	<?php //print_r($info); ?>
	<?php echo form_open('manager/storeDiscountRate'); ?>
	<?php include('messages.php');?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Maximum Amount </label>
								<input type="text" class="form-control" name="maximumAmount" value=""/>
								<div class="errorClass"><?php echo form_error('maximumAmount'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Minimum Amount </label>
								<input type="text" class="form-control" name="minimumAmount" value=""/>
								<div class="errorClass"><?php echo form_error('minimumAmount'); ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Discount Rate </label>
								<input type="text" class="form-control" name="discountPercentage" value=""/>
								<div class="errorClass"><?php echo form_error('discountPercentage'); ?></div>
							</div>
						</div>
					
				        <div class="col-md-12">
							<div class="form-group">
								<label>Note</label>
		                        <textarea name="discountNote" class="form-control"></textarea>
		                        <div class="errorClass"><?php echo form_error('discountNote'); ?></div> 
							</div>
						</div>
					</div>
				</div>
				</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary"> Add Discount Rate
							</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php') ?>