<?php include('header.php') ?>
	<?php echo form_open('admin/clearStockQuantity'); ?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Reset Stock Quantity</h3>
		</div>
	</div>
	<div class="row">
      <div class="col-md-12">
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/');?>">Dash Board</a> </li>
            <li class="active">Reset Stock Quantity</li>
         </ol>
      </div>
   	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Select Group</label>
								<select class="form-control forselect2" name="groupId" id="">
									<option value="0">All</option>
									<?php foreach($groupInfo as $groupInfo): ?>
									<option value="<?php echo $groupInfo->groupID ?>"><?php echo $groupInfo->groupName ?></option>
									<?php endforeach  ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#resetStockQuantity"><i class="fa fa-refresh"></i> Reset</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	

	<div class="modal fade bs-example-modal-sm" id="resetStockQuantity" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Reset Stock Quantity</h4>
		    </div>
		    <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>Are you sure ??</h4>
		        	</div>
		        </div>
		    </div>
		    <div class="modal-footer">
		        <button type="submit" class="btn btn-primary">Yes</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
		    </div>
	    </div>
	  </div>
	</div>
	<?php echo form_close(); ?>
<?php include('footer.php') ?>