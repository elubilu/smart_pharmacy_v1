<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends MY_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('manager_model');
		if($this->session->userdata('user_role')!=4)return redirect('login/');
	}

	public function index(){
		//$cash=$this->manager_model->get_cash_amount();
		$this->load->view('manager/dashBoard');
	}
	public function createInvoice(){
		$this->load->view('manager/createInvoice');
	}
	public function returnExchange(){
		
		$this->load->view('manager/returnExchange');
	}

	/* Inventory */
	public function addProductToStock(){
		$data=$this->manager_model->get_all_product_from_stock();
		$manufacturers=$this->manager_model->get_all_manufacturer();
		$this->load->view('manager/addProductToStock',['manufacturers'=>$manufacturers,'data'=>$data]);
	}
	public function storeProductToStock(){
  		
		//print_r($data); exit();
		$this->load->library('cart');
		$this->load->library('form_validation');
     
        	
        	$data=$this->input->post();
        	$val=$this->cart->contents(); 
        	print_r($val); exit();
        	if($cart=$this->cart->contents()){
		  		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
				$date= $dt->format('Y-m-d 00:00:00');
				$data['productUpdateDate']=$date;

				//$data['productStatus']=1;
				//$data['productQuantity']=0;
				//$data['productSaleCounter']=0;
				$value['productId']=$data['productId'];
				$value['productQuantity']=$data['productQuantity'];
				
				$data['product_info_productId']=$data['productId'];
				$infos=$this->manager_model->get_product_quantity_by_id($data['productId']);
				//$value['productQuantity']+=$infos;
				$data['productUpdateQuantity']=$data['productQuantity'];
				$data['admin_info_productUpdateBy']=$data['clientType'];

				unset($data['productQuantity']);
				unset($data['productId']);
				unset($data['productTotalPrice']);
				unset($data['clientType']);
			}
			//print_r($data);
			//print("  ");
			//print_r($value);
			 //exit(); */
			if($this->manager_model->update_quantity($value)){
		  		if($this->manager_model->store_product_to_stock($data))
		  		{
		  			$this->session->set_flashdata('feedback_successfull', 'Added Product to Stock successfully');
		  			redirect('manager/addProductToStock');
		  		}
		  	}
	  
		
	}
	public function products(){
		$data=$this->manager_model->get_all_product();
		$this->load->view('manager/products',['data'=>$data]);
	}

	public function viewProduct($id){
		$data=$this->manager_model->get_product_info_by_id($id);
		$manufacturers=$this->manager_model->get_all_manufacturer();
		$groups=$this->manager_model->get_all_medicine_groups();
		$this->load->view('manager/viewProduct',['data'=>$data,'manufacturers'=>$manufacturers,'groups'=>$groups]);
	}

	public function updateProduct(){
		$data=$this->input->post();
		//print_r($data); exit();
		$this->load->library('form_validation');
        if ($this->form_validation->run('update_product_manager') == True ){
	  		
	  		//print_r($data); exit();
	  		if($this->manager_model->update_product($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Updated Product successfully');
	  			redirect('manager/products');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Updated product failed!');
	  		redirect('manager/viewProduct/'.$data['productId']);
	  	}
		
	}
	public function storeProduct(){
  		
		//print_r($data); exit();
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_new_product_manager') == True){
        	$data=$this->input->post();
        	//print_r($data); exit();
	  		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d 00:00:00');
			$data['productAddedDate']=$date;
			$data['productStatus']=1;
			$data['productQuantity']=0;
			$data['productSaleCounter']=0;
	  		if($this->manager_model->store_product($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Added New Product successfully');
	  			redirect('manager/products');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Added New Product failed!');
	  		redirect('manager/addNewProduct');
	  	}
		
	}

	public function addNewProduct(){
		$groups=$this->manager_model->get_all_medicine_groups();
		$manufacturers=$this->manager_model->get_all_manufacturer();
		$dosage=$this->manager_model->get_all_dosages_forms();
		//print_r($dosage); exit();
		$this->load->view('manager/addNewProduct',['groups'=>$groups,'manufacturers'=>$manufacturers,'dosage'=>$dosage]);
	}
	public function manufacturersSuppliers(){
		
			$data=$this->manager_model->get_all_manufacturer();
			$this->load->view('manager/manufacturersSuppliers',['data'=>$data]);
		
	}
	public function viewManufacturer($id){
		
			$data=$this->manager_model->get_manufacturer_by_id($id);
			$this->load->view('manager/viewManufacturer',['data'=>$data]);
		
	}
	public function storeNewManufacturer(){
  		
		//print_r($data); exit();
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_new_manufacturer') == True){
        	$data=$this->input->post();
	  		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d 00:00:00');
			$data['manufacturerAddedDate']=$date;
			$data['status']=1;
	  		if($this->manager_model->store_new_manufacturer($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Added Manufacturer successfully');
	  			redirect('manager/manufacturersSuppliers');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Added Manufacturer failed!');
	  		redirect('manager/addNewManufacturer');
		}
	}
	
	
	public function updateManufacturer(){
  		
  		//print_r($data); exit();
  		$data=$this->input->post();
  		$this->load->library('form_validation');
        if ($this->form_validation->run('update_manufacturer') == True){
        	
	  		if($this->manager_model->update_manufacturer($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Updated Manufacturer successfully');
	  			redirect('manager/manufacturersSuppliers');
	  		}
  		}
  		else {
  			 $this->session->set_flashdata('feedback_failed', 'Updated Manufacturer failed!'); 			
  			redirect('manager/viewManufacturer/'.$data['manufacturerId']);
  		}
		
	}
	public function addNewManufacturer(){
		$this->load->view('manager/addNewManufacturer');
	}
	public function medicineGroup(){
		$data=$this->manager_model->get_all_medicine_groups();
		$this->load->view('manager/medicineGroup',['data'=>$data]);
	}
	public function viewMedicineGroup($id){
		$data=$this->manager_model->get_medicine_group_by_id($id);
		$this->load->view('manager/viewMedicineGroup',['data'=>$data]);
	}
	public function addNewMedicineGroup(){
		
			$this->load->view('manager/addNewMedicineGroup');

	}
    function add()
	{
		$this->load->library('cart');
		$data = array(
			'id' => $_POST["product_id"], 
			'name' => $_POST["product_name"], 
			'qnty' => $_POST["product_qnty"], 
			'client_id' => $_POST["client_id"], 
			'client_name' => $_POST["client_name"] 
			);
		$this->cart->insert($data);
		echo $this->view();
	}
	function view()
	{
		$this->load->library('cart');
		$output='';
		$output .= '

		<table class="table table-striped">
   						<thead>
   							<tr id="tableHeaderID">
                           		<th>Manufacturer </th>
   								<th>Product </th>
   								<th>Quantity </th>
   								<th>Remove</th>
   							</tr>
   						</thead>

		';
		$count=0;
		if($cart=$this->cart->contents()){
			foreach($cart as $item): 
				$count++;
				$output.='
				<tr>
					<td>'.$item['client_name'].'</td>
					<td>'.$item['name'].'</td>
					<td>'.$item['qnty'].'</td>
					<td><button type="button" name="remove" class="btn btn-danger btn-xs remove_inventory" id="'.$item['rowid'].'">Remove</button></td>
				</tr>
			';
			endforeach ;
			return $output;
		}
	}
	public function storeMedicineGroup(){
  		
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_new_medicine_group') == True){
		//print_r($data); exit();
        	$data=$this->input->post();
	  		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d 00:00:00');
			$data['medicineGroupAddedDate']=$date;
			$data['status']=1;
		  	if($this->manager_model->store_medicine_group($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Added Medicine Group successfully');
	  			redirect('manager/medicineGroup');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Added Medicine Group failed!');
	  		redirect('manager/addNewMedicineGroup');
		}
	}
	
  public function updateMedicineGroup(){
  		$data=$this->input->post();
  		//print_r($data); exit();
  		$this->load->library('form_validation');
        if ($this->form_validation->run('update_medicine_group') == True ){
	  		if($this->manager_model->update_medicine_group($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Updated Medicine Group successfully');
	  			redirect('manager/medicineGroup');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Updating Medicine Group failed!');
	  		redirect('manager/viewMedicineGroup/'.$data['medicineGroupId']);
	  	}
		
	}
	
	public function dosageForms(){
		$data=$this->manager_model->get_all_dosage_forms();
		$this->load->view('manager/dosageForms',['data'=>$data]);
	}
	public function viewDosageForms($id){
		$data=$this->manager_model->get_dose_info_by_id($id);
		$this->load->view('manager/viewDosageForms',['data'=>$data]);
	}
	public function addNewDosageForms(){
		
		$this->load->view('manager/addNewDosageForms');
	}
	public function storeDosageForms(){
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_dosage_forms') == True ){
	  		$data=$this->input->post();
	  		$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d 00:00:00');
			$data['doseAddedDate']=$date;
			$data['status']=1;
			//print_r($data); exit();
	  		if($this->manager_model->store_dosage_forms($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Added Dosage Form successfully');
	  			redirect('manager/dosageForms');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Added  Dosage Forms failed!');
	  		redirect('manager/addNewDosageForms');
	  	}
		
	}
	public function updateDosageForms(){
		$data=$this->input->post();
		$this->load->library('form_validation');
        if ($this->form_validation->run('update_dosage_forms') == True ){
	  		
	  		//print_r($data); exit();
	  		if($this->manager_model->update_dosage_forms($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Updated Dosage Form successfully');
	  			redirect('manager/dosageForms');
	  		}
	  	}
	  	else {
	  		$this->session->set_flashdata('feedback_failed', 'Updating Dosage Form failed!');
	  		redirect('manager/viewDosageForms/'.$data['doseId']);
	  	}
		
	}



	/* Other */
	public function allUser(){
		$this->load->view('manager/allUser');
	}
	public function addUser(){
		$this->load->view('manager/addUser');
	}
	public function viewUser(){
		$this->load->view('manager/viewUser');
	}
	public function discountRate(){
		$data=$this->manager_model->get_all_discount_rate();
		$this->load->view('manager/discountRate',['data'=>$data]);
	}
	public function addDiscountRate(){
		$this->load->view('manager/addDiscountRate');
	}
	public function viewDiscountRate($id){
		$data=$this->manager_model->get_discount_rate_by_id($id);
		$this->load->view('manager/viewDiscountRate',['data'=>$data]);
	}
	public function storeDiscountRate(){
  		
		//print_r($data); exit();
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_discount_rate') == True){
        	$data=$this->input->post();
	  		/*$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$date= $dt->format('Y-m-d 00:00:00');
			$data['manufacturerAddedDate']=$date;*/
			$data['status']=1;
	  		if($this->manager_model->store_discount_rate($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Add Discount Rate successfully');
	  			redirect('manager/discountRate');
	  		}
	  	}
	  	else 
	  		{
	  			$this->session->set_flashdata('feedback_failed', 'Added Discount Rate failed!');
	  			redirect('manager/addDiscountRate');
	  		}
		
	}
	public function updateDiscountRate(){
  		
		$this->load->library('form_validation');
        if ($this->form_validation->run('add_discount_rate') == True){
        	$data=$this->input->post();
	  		//print_r($data); exit();
	  		if($this->manager_model->update_discount_rate($data))
	  		{
	  			$this->session->set_flashdata('feedback_successfull', 'Updated Discount Rate successfully');
	  			redirect('manager/discountRate');
	  		}
	  		
	  	}
	  	else
	  	{
	  		$data=$this->input->post();
	  		$this->session->set_flashdata('feedback_failed', 'Update Discount Rate  Failed!');
	  		redirect('manager/viewDiscountRate/'.$data['discountRateId']);

	  	} 
		
	}
	public function returnPercentageRate(){
		$this->load->view('manager/returnPercentageRate');
	}	

	/* Report */
	public function productReport(){
		$this->load->view('manager/productReport');
	}
	public function saleReport(){
		$this->load->view('manager/saleReport');
	}
	public function stockReport(){
		$this->load->view('manager/stockReport');
	}
	public function stockEntryReport(){
		$this->load->view('manager/stockEntryReport');
	}
	public function log(){
		$this->load->view('manager/log');
	}	




	public function myAccount(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		$this->load->view('manager/myAccount',['info'=>$user]);
	}
	public function updateContact(){
		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		if($user){
			//$this->load->view('super_admin/header',['info'=>$user]);
			$data['adminContact']=$this->input->post('contact');
			if($this->manager_model->update_contact($admin_id,$data)){
				$this->session->set_flashdata('feedback_successfull', 'Updated contact successfully');
				return redirect('manager/myAccount');
			}
			else {
				$this->session->set_flashdata('feedback_failed', 'Updating contact failed!');
				return redirect('manager/myAccount');
			}
		}
		else
			return redirect('login/');
	}	
	public function updatePassword(){

		$admin_id=$this->session->userdata('admin_id');
		$user=$this->manager_model->my_info($admin_id);
		if($user){
			$this->load->library('form_validation');
			if($this->form_validation->run('update_password')){
				$data=$this->input->post();
				//print_r($data); print_r($user); exit;
				if($user->adminPassword==$data['old_pass']){
					$password['adminPassword']=$data['new_pass'];
					//print_r($password); exit;
					if($this->manager_model->change_password($password,$admin_id)){
							//echo "user Add Successful";
							$this->session->set_flashdata('feedback_successfull', 'Changed Password Successfully');
							return redirect('login/logout');
					}
					else{
						$this->session->set_flashdata('feedback_failed', 'Failed to Change Password');
						return redirect('manager/myAccount');
					}

				}
				else{
					$this->session->set_flashdata('feedback_failed', 'Old Password is not Matching ');
					return redirect('manager/myAccount');
				}

			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Please Try Again');
				$this->load->view('manager/myAccount',['info'=>$user]);
			}
		}
		else
			return redirect('login/');
	}	
}
