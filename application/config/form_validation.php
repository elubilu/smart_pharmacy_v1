<?php 
$config = array(
       'login_form' => array(
            array(
			   'field' => 'email',
               'label' => 'User Email',
               'rules' => 'required' 
            ),
            array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|md5', 
            )
               
		),
		'add_user' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[adminPassword]', 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			
			
		),
		'update_user' => array(
            array(
               'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
         array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[adminPassword]', 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
         array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
		),
		
		'add_sales_man' => array(
            array(
			   'field' => 'salesmanName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'salesmanEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            
            array(
               'field' => 'salesmanContact',
               'label' => 'Contact',
               'rules' => 'required|numeric', 
            ),
			array(
               'field' => 'salesmanAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => 'required', 
            ),
		),
		
		'update_sales_man' => array(
            array(
			   'field' => 'salesmanName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'salesmanEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            
            array(
               'field' => 'salesmanContact',
               'label' => 'Contact',
               'rules' => 'required|numeric', 
            ),
			array(
               'field' => 'salesmanAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => 'required', 
            ),
		),
		
		'add_supplier' => array(
            array(
			   'field' => 'supplierCompanyName',
               'label' => 'Company Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'supplierContactName',
               'label' => 'Contact Name',
               'rules' => 'required' 
            ),
            
            array(
               'field' => 'supplierContact',
               'label' => 'Contact Number',
               'rules' => 'required', 
            ),
			array(
               'field' => 'supplierAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
		),
	'add_new_product_manager' => array(
         array(
            'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
         array(
            'field' => 'productBarcode',
               'label' => 'Barcode',
               'rules' => 'required' 
            ),
            array(
            'field' => 'group_info_productGroupId',
               'label' => 'Group',
               'rules' => 'required' 
            ),
         array(
            'field' => 'manufacturer_info_productManufacturerId',
               'label' => 'Manufacturer',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Purchase Price',
               'rules' => 'required', 
            ),
         array(
               'field' => 'productMRP',
               'label' => 'MRP',
               'rules' => 'required', 
            ),
         
         array(
               'field' => 'productContains',
               'label' => 'Contains',
               'rules' => 'required', 
            ),  
              array(
               'field' => 'dosage_info_productDosageFormId',
               'label' => 'Dosage Form',
               'rules' => 'required', 
            ), 
       
      ),
   'update_product_manager' => array(
         array(
               'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
       
         array(
               'field' => 'group_info_productGroupId',
               'label' => 'Group',
               'rules' => 'required' 
            ),
         array(
               'field' => 'manufacturer_info_productManufacturerId',
               'label' => 'Manufacturer',
               'rules' => 'required' 
            ),
          array(
               'field' => 'productPurchasePrice',
               'label' => 'Purchase Price',
               'rules' => 'required', 
            ),
         array(
               'field' => 'productMRP',
               'label' => 'MRP',
               'rules' => 'required', 
            ),
         
         array(
               'field' => 'productContains',
               'label' => 'Contains',
               'rules' => 'required', 
            ),  
         array(
               'field' => 'dosage_info_productDosageFormId',
               'label' => 'Dosage Form',
               'rules' => 'required', 
            ),       
      ),
 'add_new_product_keeper' => array(
         array(
               'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
       
            array(
               'field' => 'group_info_productGroupID',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'supplier_info_productSupplierID',
               'label' => 'Supplier Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Purchase Price',
               'rules' => 'required', 
            ),
         array(
               'field' => 'productSalePrice',
               'label' => 'Sale Price',
               'rules' => 'required', 
            ),
      
      ), 
 'add_discount_rate' => array(
			array(
               'field' => 'minimumAmount',
               'label' => 'Minimum Amount',
               'rules' => 'required' 
            ),
       
            array(
               
			      'field' => 'maximumAmount',
               'label' => 'Maximum Amount',
               'rules' => 'required' 
            ),
			array(
			      'field' => 'discountPercentage',
               'label' => 'Discount Percentage',
               'rules' => 'required' 
            ),
            array(
               'field' => 'discountNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),
			
      
		),
   'add_product_to_stock_manager' => array(
         array(
               'field' => 'clientType',
               'label' => 'Select Manufacturer / Supplier',
               'rules' => 'required' 
            ),
         array(
               'field' => 'productQuantity',
               'label' => 'Quantity',
               'rules' => 'required' 
            ),
         array(
               'field' => 'productId',
               'label' => 'Product',
               'rules' => 'required', 
            ),
      ),  
   'add_product_to_stock_keeper' => array(
         array(
               'field' => 'productGroupName',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'productQuantity',
               'label' => 'Stock Quantity',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Purchase Price',
               'rules' => 'required', 
            ),
         array(
               'field' => 'productSalePrice',
               'label' => 'Sale Price',
               'rules' => 'required', 
            ),
         array(
               'field' => 'productSupplierName',
               'label' => 'Supplier',
               'rules' => 'required', 
            ),
      ),      
       'add_new_manufacturer' => array(
         array(
               'field' => 'manufacturerCompanyName',
               'label' => 'Company Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'manufacturerRepresentative',
               'label' => 'Representtative',
               'rules' => 'required' 
            ),
            array(
               'field' => 'manufacturerRepresentativeContact',
               'label' => 'Representtative Contact',
               'rules' => 'required', 
            ),
         array(
               'field' => 'manufacturerAddress',
               'label' => 'Company Address',
               'rules' => 'required', 
            ),
      ),
       'update_manufacturer' => array(
         array(
               'field' => 'manufacturerCompanyName',
               'label' => 'Company Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'manufacturerRepresentative',
               'label' => 'Representtative',
               'rules' => 'required' 
            ),
            array(
               'field' => 'manufacturerRepresentativeContact',
               'label' => 'Representtative Contact',
               'rules' => 'required', 
            ),
         array(
               'field' => 'manufacturerAddress',
               'label' => 'Company Address',
               'rules' => 'required', 
            ),
      ),  
      'add_new_supplier' => array(
         array(
               'field' => 'supplierCompanyName',
               'label' => 'Company Name',
               'rules' => 'required' 
            ),
         array(
               'field' => 'supplierContactName',
               'label' => 'Contact Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'supplierContact',
               'label' => 'Supplier Contact',
               'rules' => 'required', 
            ),
         array(
               'field' => 'supplierAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
      ),		
      'add_new_medicine_group' => array(
         array(
            'field' => 'medicineGroupName',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
         array(
            'field' => 'medicineGroupContains',
               'label' => 'Contains',
               'rules' => 'required' 
            ),
            array(
               'field' => 'medicineGroupNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),
         
      ),
       'update_medicine_group' => array(
			array(
			   'field' => 'medicineGroupName',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'medicineGroupContains',
               'label' => 'Contains',
               'rules' => 'required' 
            ),
            array(
               'field' => 'medicineGroupNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),
			
		),
		'update_product' => array(
			array(
			   'field' => 'productName',
               'label' => 'Product Name',
               'rules' => 'required' 
            ),
            array(
			   'field' => 'group_info_productGroupID',
               'label' => 'Group Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'supplier_info_productSupplierID',
               'label' => 'Supplier Name',
               'rules' => 'required' 
            ),
            array(
               'field' => 'productPurchasePrice',
               'label' => 'Product Purchase Price',
               'rules' => 'required', 
            ),
			array(
               'field' => 'productSalePrice',
               'label' => 'Product Sale Price',
               'rules' => 'required', 
            ),
			array(
			   'field' => 'productQuantity',
               'label' => 'Quantity',
               'rules' => '' 
            ),
			array(
			   'field' => 'productStatus',
               'label' => 'Status',
               'rules' => 'required' 
            ),
		),
		
		'add_storeKeeper' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminPassword',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[adminPassword]', 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminUserID',
               'label' => 'User ID',
               'rules' => 'required', 
            ),
			array(
               'field' => 'admin_role_roleID',
               'label' => 'User Type',
               'rules' => 'required', 
            ),
		),
		
		'update_storeKeeper' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminStatus',
               'label' => 'Status',
               'rules' => '', 
            ),
		),
		
		'update_manager' => array(
            array(
			   'field' => 'adminName',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'adminEmail',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'adminContact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminAddress',
               'label' => 'Address',
               'rules' => 'required', 
            ),
			array(
               'field' => 'adminStatus',
               'label' => 'Status',
               'rules' => '', 
            ),
			array(
               'field' => 'shop_info_shopID',
               'label' => 'Shop Name',
               'rules' => '', 
            ),
		),
		
		/*'edit_user' => array(
            array(
			   'field' => 'name',
               'label' => 'Full Name',
               'rules' => 'required' 
            ),
			array(
			   'field' => 'email',
               'label' => 'Email Address',
               'rules' => 'required|valid_email' 
            ),
            array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
            array(
               'field' => 'contact',
               'label' => 'Contact',
               'rules' => 'required', 
            ),
			array(
               'field' => 'admin_role_role_id',
               'label' => 'Role',
               'rules' => 'required', 
            ),
		),*/
		
		'update_password' => array(
            array(
               'field' => 'old_pass',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
         array(
               'field' => 'new_pass',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
         array(
               'field' => 'confirm_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[new_pass]', 
            ),
      ),
      'add_dosage_forms' => array(
            array(
               'field' => 'doseForm',
               'label' => 'Dosage Form',
               'rules' => 'required', 
            ),
         array(
               'field' => 'dosageNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),
         
      ), 
      'update_dosage_forms' => array(
            array(
               'field' => 'doseForm',
               'label' => 'Dosage Form',
               'rules' => 'required', 
            ),
         array(
               'field' => 'dosageNote',
               'label' => 'Note',
               'rules' => 'required', 
            ),
         
      ),
    
	'update_customer_password' => array(
            
			array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
			array(
               'field' => 're_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
		),
   'contact_us' => array(
            
         array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required|min_length[8]|md5', 
            ),
         array(
               'field' => 're_pass',
               'label' => 'Confirm Password',
               'rules' => 'required|min_length[8]|md5|matches[password]', 
            ),
      ),
   	'add_product_group_keeper' => array(
            
			array(
               'field' => 'groupName',
               'label' => 'Group Name',
               'rules' => 'required', 
            ),
			
		),
	);
	
	
	
?>	
	