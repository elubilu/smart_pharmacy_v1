-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2017 at 07:31 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_pharmacy_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `adminID` int(11) NOT NULL,
  `adminName` varchar(40) COLLATE utf8_bin NOT NULL,
  `adminEmail` varchar(50) COLLATE utf8_bin NOT NULL,
  `adminContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `adminAddress` text COLLATE utf8_bin NOT NULL,
  `adminUserID` varchar(30) COLLATE utf8_bin NOT NULL,
  `adminPassword` varchar(400) COLLATE utf8_bin NOT NULL,
  `adminStatus` int(2) NOT NULL DEFAULT '1' COMMENT '0=InActive, 1=Active',
  `admin_role_roleID` int(11) NOT NULL,
  `shop_info_shopID` int(11) NOT NULL,
  `adminNote` text COLLATE utf8_bin NOT NULL,
  `adminJoinDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `adminUpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`adminID`, `adminName`, `adminEmail`, `adminContact`, `adminAddress`, `adminUserID`, `adminPassword`, `adminStatus`, `admin_role_roleID`, `shop_info_shopID`, `adminNote`, `adminJoinDate`, `adminUpdateDate`) VALUES
(1, 'SatrlabIT', 'starlabTeam@gmail.com', '01719450855', 'Zindabazar,Sylhet', 'starlabIT', 'd7ea52fb792bed01df7174c48605cf19', 1, 1, 1, '', '2017-02-14 17:06:16', '0000-00-00 00:00:00'),
(6, 'Excellent Store Keeper', 'storekeeper_excellent@starlabit.email', 'Rifat', 'Excellent 631ea72176464e6d2b8adda6c47a993a\n', 'stock', 'd7ea52fb792bed01df7174c48605cf19', 1, 3, 3, '', '2017-05-03 12:05:12', '0000-00-00 00:00:00'),
(7, 'admin admin', 'admin@excellent.com', '123456', 'address', 'StarLabIT1', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, 3, '', '2017-03-22 12:27:38', '0000-00-00 00:00:00'),
(8, 'Mr. Rifat', 'rifat@jonota.com', '01727414166', 'Zindabazar', 'admin', 'd7ea52fb792bed01df7174c48605cf19', 1, 2, 3, '', '2017-06-30 19:24:59', '0000-00-00 00:00:00'),
(9, 'Excellent - 1 Manager', 'excellent1@starlabit.email', '123456', 'Zindabazar', 'manager', 'd7ea52fb792bed01df7174c48605cf19', 1, 4, 4, '', '2017-08-30 18:11:39', '2017-04-27 19:11:08'),
(10, 'Salim Shoes Manager', 'salimshoes@gmail.com', '12345678979654', 'Zindabazar, Sylhet', 'salim', 'd7ea52fb792bed01df7174c48605cf19', 1, 4, 5, '', '2017-08-30 18:11:27', '0000-00-00 00:00:00'),
(11, 'Excellent - 2 Manager', 'excellent2@starlab.email', '0123456789', 'Zindabazar', 'excellent2', '29ce822e8b7e51a318e382b0e34f4360', 1, 4, 6, '', '2017-04-27 13:09:18', '0000-00-00 00:00:00'),
(12, 'name', 'email@gmail.com', '2453557', 'address', 'userId', '5dbab745ca7dc1a1abb2e4352bdb0549', 1, 4, 4, '', '2017-08-03 16:29:52', '0000-00-00 00:00:00'),
(13, 'name', '', '2453557', '', 'manager1', '25d55ad283aa400af464c76d713c07ad', 1, 4, 0, 'Note', '2017-09-20 20:26:20', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`roleID`, `roleName`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(4, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `discount_rate_info`
--

CREATE TABLE `discount_rate_info` (
  `discountRateId` int(20) NOT NULL,
  `minimumAmount` double NOT NULL,
  `maximumAmount` double NOT NULL,
  `discountPercentage` double NOT NULL,
  `discountNote` text COLLATE utf8_bin NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `dosage_info`
--

CREATE TABLE `dosage_info` (
  `doseId` int(20) NOT NULL,
  `doseForm` varchar(100) COLLATE utf8_bin NOT NULL,
  `doseAddedDate` datetime NOT NULL,
  `dosageNote` text COLLATE utf8_bin NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `dosage_info`
--

INSERT INTO `dosage_info` (`doseId`, `doseForm`, `doseAddedDate`, `dosageNote`, `status`) VALUES
(1, 'Gas', '2017-11-06 00:00:00', 'ho', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer_info`
--

CREATE TABLE `manufacturer_info` (
  `manufacturerId` int(20) NOT NULL,
  `manufacturerCompanyName` varchar(100) COLLATE utf8_bin NOT NULL,
  `manufacturerRepresentative` varchar(100) COLLATE utf8_bin NOT NULL,
  `manufacturerRepresentativeContact` varchar(20) COLLATE utf8_bin NOT NULL,
  `manufacturerAddress` text COLLATE utf8_bin NOT NULL,
  `manufacturerAddedDate` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `manufacturer_info`
--

INSERT INTO `manufacturer_info` (`manufacturerId`, `manufacturerCompanyName`, `manufacturerRepresentative`, `manufacturerRepresentativeContact`, `manufacturerAddress`, `manufacturerAddedDate`, `status`) VALUES
(1, 'Ase', 'gupon', 'nai', 'yuiy', '2017-11-07 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `medicine_group`
--

CREATE TABLE `medicine_group` (
  `medicineGroupId` int(10) NOT NULL,
  `medicineGroupName` varchar(100) COLLATE utf8_bin NOT NULL,
  `medicineGroupContains` varchar(200) COLLATE utf8_bin NOT NULL,
  `medicineGroupNote` text COLLATE utf8_bin NOT NULL,
  `medicineGroupAddedDate` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `medicine_group`
--

INSERT INTO `medicine_group` (`medicineGroupId`, `medicineGroupName`, `medicineGroupContains`, `medicineGroupNote`, `medicineGroupAddedDate`, `status`) VALUES
(1, 'Bang', 'BAngladesh', '5', '2017-11-06 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `return_discount_rate_info`
--

CREATE TABLE `return_discount_rate_info` (
  `Id` int(10) NOT NULL,
  `currentReturnValueDeductionRate` double NOT NULL,
  `returnPercentage` double NOT NULL,
  `status` int(2) NOT NULL,
  `returnDiscountNote` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

CREATE TABLE `supplier_info` (
  `supplierId` int(10) NOT NULL,
  `companyName` varchar(100) COLLATE utf8_bin NOT NULL,
  `companyAddress` varchar(100) COLLATE utf8_bin NOT NULL,
  `representative` varchar(100) COLLATE utf8_bin NOT NULL,
  `representativeContact` varchar(16) COLLATE utf8_bin NOT NULL,
  `supplierAddedDate` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`adminID`),
  ADD UNIQUE KEY `adminUserID` (`adminUserID`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `discount_rate_info`
--
ALTER TABLE `discount_rate_info`
  ADD PRIMARY KEY (`discountRateId`);

--
-- Indexes for table `dosage_info`
--
ALTER TABLE `dosage_info`
  ADD PRIMARY KEY (`doseId`);

--
-- Indexes for table `manufacturer_info`
--
ALTER TABLE `manufacturer_info`
  ADD PRIMARY KEY (`manufacturerId`);

--
-- Indexes for table `medicine_group`
--
ALTER TABLE `medicine_group`
  ADD PRIMARY KEY (`medicineGroupId`);

--
-- Indexes for table `return_discount_rate_info`
--
ALTER TABLE `return_discount_rate_info`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `supplier_info`
--
ALTER TABLE `supplier_info`
  ADD PRIMARY KEY (`supplierId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `discount_rate_info`
--
ALTER TABLE `discount_rate_info`
  MODIFY `discountRateId` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dosage_info`
--
ALTER TABLE `dosage_info`
  MODIFY `doseId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `manufacturer_info`
--
ALTER TABLE `manufacturer_info`
  MODIFY `manufacturerId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `medicine_group`
--
ALTER TABLE `medicine_group`
  MODIFY `medicineGroupId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `return_discount_rate_info`
--
ALTER TABLE `return_discount_rate_info`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier_info`
--
ALTER TABLE `supplier_info`
  MODIFY `supplierId` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
